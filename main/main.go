package main

import (
	"flag"
	"fmt"
	"time"

	"gitlab.com/jacobhaven/interview-gitlab"
)

var (
	url                   string
	pingInterval, runtime time.Duration
)

func init() {
	flag.StringVar(&url, "url", "https://gitlab.com", "URL to ping")
	flag.DurationVar(&pingInterval, "interval", time.Second, "Interval between each ping")
	flag.DurationVar(&runtime, "runtime", 5*time.Minute, "Total measurement time")
	flag.Parse()
}

func main() {
	pinger := gitlab.NewPinger(url, pingInterval)
	pinger.Run(runtime)

	fmt.Printf("Average response time from %s was %v with σ = %v\n", url, time.Duration(pinger.Mean()), time.Duration(pinger.StdDev()))
	fmt.Println("Response time percentiles:")
	for _, percentile := range []float64{0, 0.5, 0.75, 0.9, 0.95, 0.99} {
		fmt.Printf("\tP%02.f%%: %v\n", percentile*100, time.Duration(pinger.Percentile(percentile)))
	}

}

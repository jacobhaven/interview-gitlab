package gitlab

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestQueue(t *testing.T) {
	testdata := []interface{}{0, 1, 2, 3, 4}

	q := NewQueue(1) // Stack of size 1 to serialize input/output, equivalent to a queue of size 1.
	go func() {
		for _, v := range testdata {
			q.Push(v)
		}
	}()
	for _, v := range testdata {
		require.Equal(t, v, q.Pop())
	}
}

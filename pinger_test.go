package gitlab

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestPinger(t *testing.T) {
	serverDelay := 5 * time.Millisecond
	server := httptest.NewServer(http.HandlerFunc(func(http.ResponseWriter, *http.Request) {
		time.Sleep(serverDelay)
	}))

	pinger := &Pinger{url: server.URL, pingInterval: 100 * time.Millisecond}
	pinger.Run(time.Second)

	// require that the Pinger mean is within 1 ns of the actual delay.
	require.InEpsilon(t, serverDelay, pinger.Mean(), 1)
}

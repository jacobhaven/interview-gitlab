package gitlab

import (
	"sync"
)

// Stack implements a basic concurrent, blocking stack.
type Stack struct {
	capacity          int
	mutex             sync.Mutex
	popCond, pushCond *sync.Cond
	st                []interface{}
}

// NewStack initializes and returns a new *Stack.
func NewStack(capacity int) *Stack {
	s := &Stack{capacity: capacity}
	s.popCond, s.pushCond = sync.NewCond(&s.mutex), sync.NewCond(&s.mutex)
	return s
}

// Push inserts an element to the top of the stack.
func (s *Stack) Push(v interface{}) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	for len(s.st) >= s.capacity {
		s.popCond.Wait()
	}
	s.st = append(s.st, v)
	s.pushCond.Signal()
}

// Pop returns the top element of the stack.
func (s *Stack) Pop() (v interface{}) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	for len(s.st) == 0 {
		s.pushCond.Wait()
	}
	v, s.st = s.st[len(s.st)-1], s.st[:len(s.st)-1]
	s.popCond.Signal()
	return v
}

package gitlab

import (
	"fmt"
	"net/http"
	"time"

	"github.com/rcrowley/go-metrics"
)

// Pinger measures the times to connect to a given URL.
type Pinger struct {
	url          string
	pingInterval time.Duration
	metrics.Timer
	errCh chan error
}

// NewPinger instantiates a new *Pinger.
func NewPinger(url string, pingInterval time.Duration) *Pinger {
	return &Pinger{url: url, pingInterval: pingInterval}
}

// ping performs one HTTP GET request and measures the time it takes.
func (pinger *Pinger) ping() {
	start := time.Now()
	_, err := http.Get(pinger.url)
	if err != nil {
		pinger.errCh <- err
	}
	pinger.UpdateSince(start)
}

// Run runs ping every pingInterval in parallel over a total given runtime.
func (pinger *Pinger) Run(runtime time.Duration) {
	pinger.Timer = metrics.NewTimer()
	pinger.errCh = make(chan error)

	ticker := time.NewTicker(pinger.pingInterval)
	endTimer := time.NewTimer(runtime)
	defer func() { ticker.Stop(); endTimer.Stop() }()

	for {
		select {
		case <-ticker.C:
			go pinger.ping()
			fmt.Print(".")
		case err := <-pinger.errCh:
			fmt.Println(err)
		case <-endTimer.C:
			fmt.Println()
			return
		}
	}
}

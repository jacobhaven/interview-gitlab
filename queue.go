package gitlab

// Queue implements a basic concurrent, blocking queue by wrapping a chan.
type Queue struct {
	ch chan interface{}
}

// NewQueue initializes and returns a new *Queue.
func NewQueue(capacity int) *Queue {
	return &Queue{make(chan interface{}, capacity)}
}

// Push inserts an element to the back of the queue.
func (q *Queue) Push(v interface{}) {
	q.ch <- v
}

// Pop returns the front element of the queue.
func (q *Queue) Pop() (v interface{}) {
	return <-q.ch
}
